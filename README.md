Pantheon-files-previewer <br>
<br>
Description : <br>
This plugin display in pantheon-files previewer. When clicking on a file a box appear to display the thumbnail and basic information about the file (name, date, etc...). <br>
<br>

```
Note :
To make this plugin work, you shloud download our fork of pantheon-files.
```

```
Requirements :
    For the execution:
         All requirements from Pantheon-files.

    For the build:
         meson
         posix
         glib-2.0
         gee-0.8
	 gtk+-3.0
	 granite
	 pantheon-files-core
	 pantheon-files-widget
```

Install :<br>
To install this plugin :<br>
<br>
```
    meson build
    cd build
    sudo ninja install
```


License :<br>
This program is released under the terms of the GNU General Public License. <br>
Please see the file COPYING for details.<br>
<br>
