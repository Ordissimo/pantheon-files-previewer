/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Files.Plugins.Previewer : Files.Plugins.Base {
    private Files.AbstractSlot slot_view;
    private Gtk.Box parent_frame = null;

    private Files.File current_gof = null;

    Files.ViewMode view_mode;

    private Gtk.Frame frame_previewer = null;
    private Gtk.Box box_previewer = null;
    private Gtk.Image image_event = null;
    private Gtk.Label filename = null;
    private Gtk.Label date = null;
    private Gtk.Label filesize = null;

    private Gtk.Adjustment hadj;

    private bool previewer_set = false;
    private int size_labels;

    private Settings app_settings;
    private Settings zoom_settings;
    private Gee.HashMap<Files.ZoomLevel, Files.IconSize> zooms;

    private const string TEMPLATE_MARKUP = "<span size='%d'>%s</span>";
    private const int SIZE_TEXT_NORMAL = 10000;
    private const int SIZE_TEXT_LARGE = 12000;
    private const int SIZE_TEXT_LARGER = 14000;
    private const int SIZE_TEXT_HUGE = 16000;
    private const int SIZE_TEXT_HUGER = 18000;
    private const int SIZE_TEXT_LARGEST = 20000;

    public Previewer () {
        /* Initiliaze gettext support */
        Intl.setlocale (LocaleCategory.ALL, "");

        construct_previewer ();

        zooms = new Gee.HashMap<Files.ZoomLevel, Files.IconSize> ();
        zooms.@set (Files.ZoomLevel.SMALLEST, Files.IconSize.NORMAL);
        zooms.@set (Files.ZoomLevel.SMALLER, Files.IconSize.LARGE);
        zooms.@set (Files.ZoomLevel.SMALL, Files.IconSize.LARGER);
        zooms.@set (Files.ZoomLevel.NORMAL, Files.IconSize.HUGE);
        zooms.@set (Files.ZoomLevel.LARGE, Files.IconSize.HUGER);
        zooms.@set (Files.ZoomLevel.LARGER, Files.IconSize.LARGEST);

        app_settings  = new Settings ("io.elementary.files.preferences");

        zoom_settings  = new Settings ("io.elementary.files.column-view");
        Files.ZoomLevel current_zoom = (Files.ZoomLevel)zoom_settings.get_enum ("zoom-level");

        size_labels = zoom_level_to_label_size (current_zoom);
    }

    private int zoom_level_to_label_size (Files.ZoomLevel zoom_level) {
        switch (zoom_level) {
            case ZoomLevel.SMALLEST:
                return SIZE_TEXT_NORMAL;
            case ZoomLevel.SMALLER:
                return SIZE_TEXT_LARGE;
            case ZoomLevel.SMALL:
                return SIZE_TEXT_LARGER;
            case ZoomLevel.NORMAL:
                return SIZE_TEXT_HUGE;
            case ZoomLevel.LARGE:
                return SIZE_TEXT_HUGER;
            case ZoomLevel.LARGER:
                return SIZE_TEXT_LARGEST;
            default:
                warning ("Could not find the new size label, set to normal");
                return SIZE_TEXT_NORMAL;
        }
    }

    private void get_factor_thumbnail (int pixbuf_size,
                                       int width, int height,
                                       out int factored_width, out int factored_height) {
        float factor;

        if (width > height)
            factor = (float) pixbuf_size / (float)width;
        else
            factor = (float) pixbuf_size / (float)height;

        factored_width = (int)(width * factor);
        factored_height = (int)(height * factor);

        factored_width = int.max (factored_width, 1);
        factored_height = int.max (factored_height, 1);

        return;
    }

    private void  update_pixbuf_previewer () {
        GLib.return_if_fail (current_gof != null);

        Gdk.Pixbuf? pix = null;
        int width_previewer;
        int pixbuf_size;

        width_previewer = frame_previewer.get_allocated_width ();
        if (width_previewer <= 1)
            width_previewer = (int)slot_view.overlay.get_allocated_width () / 3;

        pixbuf_size = (int)(width_previewer * 0.65);

        try {
            GLib.FileInputStream file_read = current_gof.location.read ();

            pix = new Gdk.Pixbuf.from_stream_at_scale (file_read, pixbuf_size, pixbuf_size, true);
            pix = pix.apply_embedded_orientation ();
            file_read.close ();
        } catch (Error e) {
            debug ("Failed to get pixbuf from file %s with error %s.\n Trying to get icon instead.", current_gof.uri, e.message);
            pix = current_gof.get_icon_pixbuf (256, box_previewer.get_scale_factor (), Files.File.IconFlags.USE_THUMBNAILS);

            int fwidth, fheight;
            get_factor_thumbnail (pixbuf_size, pix.width, pix.height, out fwidth, out fheight);
            pix = pix.scale_simple (fwidth, fheight, Gdk.InterpType.HYPER);
        }

        if (pix != null) {
            GLib.Idle.add ( () => {
                image_event.set_from_pixbuf (pix);
                return GLib.Source.REMOVE;
            });
        }
    }

    private void update_labels_previewer () {
        string label;
        label = GLib.Markup.printf_escaped (TEMPLATE_MARKUP, size_labels, current_gof.basename);
        filename.set_markup (label);

        if (current_gof.formated_modified != null) {
            label = GLib.Markup.printf_escaped (TEMPLATE_MARKUP, size_labels, current_gof.formated_modified);
            date.set_markup (label);
        }

        if (current_gof.format_size != null) {
            label = GLib.Markup.printf_escaped (TEMPLATE_MARKUP, size_labels, current_gof.format_size);
            filesize.set_markup (label);
        }
    }

    private void update_previewer  (bool update_labels = true, bool update_pixbuf = true) {
        GLib.return_if_fail (current_gof != null);

        if (update_pixbuf)
            update_pixbuf_previewer ();

        if (update_labels)
            update_labels_previewer ();
    }

    private bool open_file () {
        if (current_gof == null)
            return false;

        unowned string? ftype = current_gof.get_ftype ();
        if (ftype.contains ("audio"))
            return false;

#if USE_XDG_OPEN
        try {
            GLib.Process.spawn_command_line_async ("xdg-open " + current_gof.uri);
        } catch (SpawnError e) {
            warning ("Could not spawn xdg-open with the file %s", current_gof.uri);
        }
#else
        AppInfo app = GLib.AppInfo.get_default_for_type (ftype, true);//file.get_default_handler ();
        GLib.List <GLib.File> list_files = new GLib.List <GLib.File>();
        list_files.append (current_gof.location);
        try {
            app.launch (list_files, null);
        } catch (Error e) {
            warning ("Cannot open image %s from previewer with GLib.AppInfo.get_default_for_type . Error : %s", current_gof.uri, e.message);
        }
#endif
        return false;
    }

    private void construct_previewer () {
        frame_previewer = new Gtk.Frame (null);
        frame_previewer.get_style_context ().remove_class (Gtk.STYLE_CLASS_FRAME);

        box_previewer = new Gtk.Box (Gtk.Orientation.VERTICAL, 10) {
            halign=Gtk.Align.CENTER,
            valign=Gtk.Align.CENTER
        };

        Pango.AttrList attrs = new Pango.AttrList ();
        attrs.insert (Pango.attr_weight_new (Pango.Weight.NORMAL));

        filename = new Gtk.Label (null) {
            ellipsize=Pango.EllipsizeMode.END,
            max_width_chars=40
        };

        image_event = new Gtk.Image () {
            halign=Gtk.Align.CENTER
        };

        Gtk.EventBox icon_event = new Gtk.EventBox () {
            visible_window=false
        };
        icon_event.set_name ("frame-previewer");
        icon_event.enter_notify_event.connect ( () => {
            Gdk.Cursor pointer = new Gdk.Cursor.from_name (Gdk.Display.get_default(), "pointer");
            ((Gdk.Window)icon_event.get_parent_window()).set_cursor (pointer);
            return true;
        });

        icon_event.leave_notify_event.connect ( () => {
            ((Gdk.Window)icon_event.get_parent_window()).set_cursor (null);
            return true;
        });
        icon_event.button_release_event.connect (open_file);
        icon_event.add (image_event);

        date = new Gtk.Label (null) {
            attributes=attrs,
            use_markup=true
        };
        filesize = new Gtk.Label (null) {
            attributes=attrs,
            use_markup=true
        };

        box_previewer.add (icon_event);
        box_previewer.add (filename);
        box_previewer.add (date);
        box_previewer.add (filesize);

        frame_previewer.add (box_previewer);

        GLib.Idle.add ( () =>  {
            int previewer_min_width;
            previewer_min_width = (slot_view.overlay.get_allocated_width () / 3) + 2;
            frame_previewer.set_size_request (previewer_min_width, -1);

            return GLib.Source.REMOVE;
        });
    }

    private void add_previewer_in_box_slot () {
        Gtk.Overlay overlay = slot_view.overlay;
        var scrolledwindow = ((Gtk.Bin)overlay).get_child();
        var viewport = ((Gtk.Bin)scrolledwindow).get_child();
        if ( (viewport is Gtk.Bin) == false)
            return;

        construct_previewer ();

        parent_frame = ((Gtk.Bin)viewport).get_child() as Gtk.Box;
        parent_frame.pack_end (frame_previewer, true, true);
        hadj = ((Gtk.ScrolledWindow)scrolledwindow).get_hadjustment ();
    }

    //When changing view, the emptybox reappear
    // which block the expand of the slot;
    private void hide_empty_box () {
        GLib.List<weak Gtk.Widget> children;
        Gtk.Container child_parent_frame;
        Gtk.Container empty_box;

        children = parent_frame.get_children ();
        if (children != null) {
            child_parent_frame = children.data as Gtk.Container;

            children = child_parent_frame.get_children ();
            if (children != null) {
                empty_box = children.last ().data as Gtk.Container;
                empty_box.set_visible (false);
            }
        }
    }

    private bool on_key_zoom_event (Gdk.EventKey event) {
        if (view_mode != Files.ViewMode.MILLER_COLUMNS || !frame_previewer.get_visible ())
            return Gdk.EVENT_STOP;

        Gdk.ModifierType modifiers = Gtk.accelerator_get_default_mod_mask ();
        bool control_pressed = ((event.state & modifiers) == Gdk.ModifierType.CONTROL_MASK);
        if (!control_pressed)
            return Gdk.EVENT_STOP;

        switch (event.keyval) {
            case Gdk.Key.KP_Subtract:
            case Gdk.Key.minus:
            case Gdk.Key.KP_Add:
            case Gdk.Key.plus:
                Files.ZoomLevel current_zoom = (Files.ZoomLevel)zoom_settings.get_enum ("zoom-level");
                Files.ZoomLevel maximum_zoom = (Files.ZoomLevel)zoom_settings.get_enum ("maximum-zoom-level");

                if (current_zoom > 0 && current_zoom < maximum_zoom) {
                    size_labels = zoom_level_to_label_size (current_zoom);
                    update_previewer (true, false);

                    // When zooming set the file in view range;
                    GLib.List<GLib.File> locations = new GLib.List<GLib.File>();
                    locations.prepend (current_gof.location);
                    slot_view.select_glib_files (locations, current_gof.location);
                }
                break;
            default:
                break;
        }
        return Gdk.EVENT_PROPAGATE;
    }

    private void on_dir_file_changed (Files.File file) {
        // Currrent_gof may be null if we restore a file from trash;
        if (file == null || current_gof == null)
            return;

        if (current_gof.uri != file.uri) {
            // If file changed and the current file does not exists
            // It means (probably) that the file has been renamed.
            if (!current_gof.exists) {
                current_gof = file;
                update_previewer (true, false);

                string scheme = slot_view.location.get_uri_scheme ();
                bool is_trash_folder = (scheme == "trash")  || slot_view.directory.is_trash;
                if (!is_trash_folder && !frame_previewer.get_visible ())
                    frame_previewer.set_visible (true);
            }
        }
    }

    private void on_dir_file_deleted (Files.File file) {
        if (current_gof.uri == file.uri)
            GLib.Idle.add (hide_previewer_on_idle);
    }

    private void on_file_changed () {
        GLib.return_if_fail (current_gof != null);
        update_previewer (false, true);
    }

    // Avoid glitch when selection change between a file and a folder.
    private bool hide_previewer_on_idle () {
        unowned var selected_files = slot_view.get_selected_files ();
        if (selected_files == null || selected_files.length () != 1 || selected_files.data.is_directory) {
            if (frame_previewer.get_visible ())
                frame_previewer.set_visible (false);

            if (current_gof != null)
                current_gof = null;
        }
        return GLib.Source.REMOVE;
    }

    private void on_selection_changed (GLib.List<Files.File> files) {
        // It seems like a double check, but between now
        // and the call of the idle, the list change.
        if (files == null || files.length () != 1 || files.data.is_directory) {
            GLib.Timeout.add (500, hide_previewer_on_idle);
            return;
        }

        if (files.data == current_gof) {
            // Sometimes, switching between folder and file very fast,
            // we enter in this case.
            if (!frame_previewer.get_visible ()) {
                frame_previewer.set_visible (true);
                GLib.Timeout.add (100, update_and_scroll);
            }
            return;
        }

        if (!frame_previewer.get_visible ())
            frame_previewer.show_all ();

        current_gof = files.data;
        current_gof.changed.connect (on_file_changed);

        // Changed signal from file doesn't work when we rename the file so we use the signal from GOF.Directory
        var dir = slot_view.directory;
        if (dir != null) {
            dir.file_changed.connect (on_dir_file_changed);
            dir.file_deleted.connect (on_dir_file_deleted);
        }

        GLib.Timeout.add (100, update_and_scroll);
    }

    // In timeout to let the previewer
    // to be fully drawn so that we can get the real value of width.
    private bool update_and_scroll () {
        update_previewer ();

        int value_adj = (int)hadj.get_value () + frame_previewer.get_allocated_width ();
        Files.Animation.smooth_adjustment_to (hadj, value_adj);
        return GLib.Source.REMOVE;
    }

    // We create the previewer on_idle
    // to avoid seeing it at start of files before it hides.
    private bool init_on_idle () {
        add_previewer_in_box_slot  ();
        window.key_release_event.connect_after (on_key_zoom_event);
        previewer_set = true;

        /* We hide the slot */
        hide_empty_box ();

        return GLib.Source.REMOVE;
    }

    private void destroy_previewer () {
        if (frame_previewer == null)
            return;
        parent_frame.remove (frame_previewer);
        frame_previewer.destroy ();
        frame_previewer = null;
        previewer_set = false;
    }

    public override void directory_loaded (Gtk.ApplicationWindow window, Files.AbstractSlot view, Files.File directory) {
        slot_view = view;

        slot_view.selection_changed.connect_after (on_selection_changed);

        view_mode = (Files.ViewMode)app_settings.get_enum ("default-viewmode");
        if (view_mode != Files.ViewMode.MILLER_COLUMNS) {
            destroy_previewer ();
            return;
        }

        if (!previewer_set) {
            GLib.Idle.add (init_on_idle);
        } else if (frame_previewer.get_visible ()) {
            // Unset the image pixbuf to avoid seeing the change.
            image_event.set_from_pixbuf (null);
            frame_previewer.set_visible (false);
        }
    }
}

public Files.Plugins.Base module_init () {
    return new Files.Plugins.Previewer ();
}
